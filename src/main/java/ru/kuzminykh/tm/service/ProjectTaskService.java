package ru.kuzminykh.tm.service;

import ru.kuzminykh.tm.entity.Project;
import ru.kuzminykh.tm.entity.Task;
import ru.kuzminykh.tm.exception.ProjectNotFoundException;
import ru.kuzminykh.tm.exception.TaskNotFoundException;
import ru.kuzminykh.tm.repository.ProjectRepository;
import ru.kuzminykh.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Task> findAllByProjectId(final Long projectId) throws TaskNotFoundException {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }


    public Task removeTaskToProject(final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException {
        final  Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException {
        final Project project = projectRepository.findById(projectId);
        if (project == null) throw new ProjectNotFoundException("Project by id: "+  projectId.toString() + ", not found!");
        final  Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

}
