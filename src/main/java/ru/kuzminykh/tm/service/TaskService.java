package ru.kuzminykh.tm.service;

import ru.kuzminykh.tm.entity.Task;
import ru.kuzminykh.tm.exception.ProjectNotFoundException;
import ru.kuzminykh.tm.exception.TaskNotFoundException;
import ru.kuzminykh.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    public Task create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return null;
        return taskRepository.create(name, description, userId);
    }

    public Task update(final Long id, final String name, final String description) throws TaskNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

    public void clear() {
        taskRepository.clear();
    }

    public Task findByIndex(final int index) throws TaskNotFoundException {
         return taskRepository.findByIndex(index);
    }

    public List<Task> findByName(String name) throws TaskNotFoundException {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    public Task findById(final Long id) throws TaskNotFoundException {
        if (id == null) return null;
        return taskRepository.findById(id);
    }

    public Task removeByIndex(final int index) throws TaskNotFoundException {
        return taskRepository.removeByIndex(index);
    }

    public Task removeById(final Long id) throws TaskNotFoundException {
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    public List<Task> removeByName(String name) throws TaskNotFoundException {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    public List<Task> findAllByProjectId(final Long projectId) throws TaskNotFoundException {
        if (projectId == null) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task findByProjectIdAndId(Long projectId, Long id) throws ProjectNotFoundException, TaskNotFoundException {
        if (projectId == null || id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

    public List<Task> findAllByUserId(Long userId) {
        if (userId == null) return null;
        return taskRepository.findAllByUserId(userId);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

}
